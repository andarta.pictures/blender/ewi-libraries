import bpy 
import os
from bpy.app.handlers import persistent


previous_object = None

def cp_toggle_auto(asset_name) :
    #print('-----------------------------asset : ' , asset_name)
    exceptions = ['PLT_EWI_RIG', 'EWI_LIB_PLT-TASKS_MS_PLT', 'GPPLT_EWI_ANIM']
    for pal in bpy.context.scene.gpmatpalettes.palettes :
        #print('palette : ', pal.name)
        if pal.name not in exceptions :
            if asset_name == pal.name.split('_')[2] or asset_name.replace('-','_') in pal.name : 
                pal.visible = True  
            else :
                pal.visible =  False 
        else : 
            pal.visible = True
        #print(pal.visible)


@persistent
def on_object_change(scene):
    global previous_object
    preferences = bpy.context.preferences
    addon_prefs = preferences.addons[__package__].preferences
    
    current_object = bpy.context.object
    if current_object != previous_object:
        previous_object = current_object

        if current_object is not None:
            object_name = current_object.name
            if len(object_name.split('_')) > 2 :
                asset_name = object_name.split('_')[2]                
                
                if addon_prefs.auto_switch_lib and asset_name in [lib.name for lib in preferences.filepaths.asset_libraries]:  #switch library TODO : ignore if current lib is bank anim
                    for window in bpy.context.window_manager.windows:
                        for area in window.screen.areas :
                            if area.type == 'FILE_BROWSER' and area.ui_type == 'ASSETS' and area.spaces[0].params.asset_library_ref != 'BANK_ANIM' : 
                                area.spaces[0].params.asset_library_ref = asset_name
                                break

                if addon_prefs.auto_switch_pal and 'gpmatpalettes' in dir(bpy.context.scene):  #switch palettes
                    cp_toggle_auto(asset_name)

@persistent
def on_load_handler(self,context) :
    print('_____ LOAD HANDLER --- EWI_LIBS')
    addon_prefs = bpy.context.preferences.addons[__package__].preferences
    if addon_prefs and addon_prefs.auto_load_kitsu_assets :
        bpy.ops.ewilib.refresh_dic()

    libraries = bpy.context.preferences.filepaths.asset_libraries
    ewi_libs = addon_prefs.ewi_libs
    for lib in libraries :
        if lib.name not in [ewi_lib.name for ewi_lib in ewi_libs]:
            new_lib = ewi_libs.add()
            new_lib.name = lib.name
            new_lib.path = lib.path

from .operators import import_asset_lib, remove_asset_lib

def unload_bank():
    addon_prefs = bpy.context.preferences.addons[__package__].preferences
    for i, lib in enumerate(addon_prefs.ewi_libs):
        if lib.name == 'BANK_ANIM' :
            remove_asset_lib(lib)
            addon_prefs.ewi_libs.remove(i)
            return

def load_bank():
    addon_prefs = bpy.context.preferences.addons[__package__].preferences
    if 'BANK_ANIM' not in [lib.name for lib in addon_prefs.ewi_libs] :
        new_lib = addon_prefs.ewi_libs.add()
        new_lib.full_name = 'BANK_ANIM'
        new_lib.name = 'BANK_ANIM'
        #determine folderpath
        new_lib.path = os.path.join(addon_prefs.base_path, 'BANK_ANIM')
        #load asset library
        import_asset_lib(new_lib)
        preferences = bpy.context.preferences
        if addon_prefs.auto_switch_lib  :
            for window in bpy.context.window_manager.windows:
                for area in window.screen.areas :
                    if area.type == 'FILE_BROWSER' and area.ui_type == 'ASSETS': 
                        area.spaces[0].params.asset_library_ref = 'BANK_ANIM'
                        break

def load_bank_update(self,context):
    if self.load_bank :
        load_bank()
    else :
        unload_bank()
