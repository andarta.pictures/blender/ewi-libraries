import subprocess
import bpy
import json
import os
import shutil
import time

local_path = 'sources'
online_path = "\\\\srvfiles\\projects\\ewilan\\FAB"
verbose = True
LOG_LINE = 1

def clear_log_libs(log):
    log['libs'] = []
    save_log(log)
    return log

def get_log(force_creation = False, verbose=verbose) :    
    ''' Load the export log text line as a json object'''
    try :
        text_log = bpy.data.texts.get('Export Offline Log')
        if not text_log:
            if force_creation:
                text_log = write_export_log({"online_filepath" : bpy.data.filepath, "libs":[]})
            else: return None
        log = json.loads(text_log.lines[LOG_LINE].body)
        if verbose : print('Log loaded')
        return log
    except Exception as e :
        print('ERROR : export log not found : ', e)
        return None

def save_log(log, verbose=verbose) :
    ''' Save the log as a json object in the text line'''
    if verbose : print('Saving log...')
    try :
        text_log = bpy.data.texts['Export Offline Log']
        log_json = json.dumps(log)
        text_log.lines[LOG_LINE].body = log_json
        # bpy.data.texts['Export Offline Log'] = text_log

        if verbose : print('Log saved')
    except Exception as e :
        print('ERROR : export log not found : ', e)

def get_lib_log_path(lib,log,verbose=verbose):
    ''' Get the online and offline path of a library from the log'''
    for offline_lib, online_lib in log['libs']:
        if offline_lib.endswith(lib.name) :
            if verbose : print('Found lib : \n- offline path', offline_lib , '\n- online path', online_lib)
            return offline_lib, online_lib
    if verbose : print('Lib not found in log')
    return None,None

def conform_sub_library_path(verbose = verbose, is_sub_lib = False):
    #get current file parent folder folders list
    if is_sub_lib:
        #in this case it is grand parent folder
        parent_path = os.path.dirname(bpy.data.filepath)
        abs_source_folder = os.path.dirname(parent_path)
    else:
        parent_path = os.path.dirname(bpy.data.filepath)
        abs_source_folder = os.path.join(parent_path, local_path)
    folder_list = os.listdir(abs_source_folder)
    for lib in bpy.data.libraries:
        #find the parent folder of the library
        lib_folder_name =  lib.name.split('.')[0]
        if lib_folder_name in folder_list:
            if lib.name[-4:].startswith('.0'):
                if verbose : print('Auto_incremented file')
                file_name = lib.name[:-4]    
            else:
                file_name = lib.name        
            #get the new path
            if is_sub_lib:
                new_path = os.path.join('..', lib_folder_name,file_name)
            else:
                new_path = os.path.join(local_path, lib_folder_name,file_name)
            new_path = '//'+ new_path
            if new_path != lib.filepath:
                if verbose : print('Conforming path for ', lib.name, 'from', lib.filepath, 'to', new_path)
                lib.filepath = new_path
            else:
                if verbose : print('Path already conformed for ', lib.name)

        else:
            print('ERROR : Library folder not found in source for ', lib.name)
  
        pass


def conform_log_library_path(log, verbose = verbose):
    for lib in bpy.data.libraries:
        if verbose : print('\n --- Start conforming: ', lib.name,' ---')
        current_path = lib.filepath 
        
        if current_path.startswith('//') :
            blender_file_path = bpy.data.filepath
            file_root_folder = os.path.dirname(blender_file_path)
            current_path = os.path.join(file_root_folder, current_path[2:])


        offline_lib, online_lib = get_lib_log_path(lib,log)
        if offline_lib is  None :
            if verbose : print('Lib not found in log, current path :', current_path)
            #CONFORM LOCAL PATH
            new_offline_lib,is_new = conform_local_path(lib)

            #check if there isn't already a log entry with this offline_lib
            if not is_new :
                is_new = True
                for offline_lib, online_lib in log['libs']:
                    if offline_lib == new_offline_lib:
                        if verbose : print('Lib already in log')
                        is_new = False
                        break

            if is_new :
                if verbose : print('Adding new lib to log')
                #GUESS ONLINE PATH
                folder_path = get_server_folder_from_name(lib.name.split('.')[0])
                if folder_path :
                    online_lib = os.path.join(folder_path, os.path.basename(lib.filepath))
                    #ADD LINE TO LOG
                    log['libs'].append([new_offline_lib, online_lib])

        elif current_path != offline_lib :
            if verbose : print('Lib path != Log Path --> Conforming lib path to log')
            new_offline_lib,is_new = conform_local_path(lib)
            # replace log['libs] line
            for i, l in enumerate(log['libs']):
                if l[0] == offline_lib:
                    log['libs'][i] = [new_offline_lib, online_lib]
                    if verbose : print('Replacing line', l, 'by', [new_offline_lib, online_lib])
                    break
        if verbose : print(lib.name,' Lib conforming done')

    save_log(log)
    if verbose : print('All libs are conformed and written in log')
                

def conform_local_path(lib, verbose = verbose):
    ''' Check if lib local path is expected
    If not, do a local copy of the file
    return local_path
    '''
    if verbose : print('Conforming local path for', lib.name)

    blender_file_path = bpy.data.filepath
    file_root_folder = os.path.dirname(blender_file_path)

    
    current_path = lib.filepath
    if current_path.startswith("//"):
        if verbose : print('--Current path is relative')
        abs_current_path = os.path.join(file_root_folder, current_path[2:])
    else :
        if verbose : print('--Current path is absolute')
        abs_current_path = current_path

    expected_path = get_lib_expected_local_paths(current_path,file_root_folder)
    is_new = False

    if abs_current_path != expected_path :
        if verbose : print('-Conforming:\n--', abs_current_path, '\nto\n--', expected_path)

        if not os.path.exists(expected_path) :
            if verbose : print('--File does not exist, copying')
            try :
                os.makedirs(os.path.dirname(expected_path), exist_ok=True)
                # shutil.copy2(abs_current_path, expected_path)
                subprocess_safe_copy(abs_current_path, expected_path, verbose = verbose)
                is_new = True
                if verbose : print('--Copy done')
            except Exception as e :
                print('ERROR : copy failed', e)
                return None
        else: 
            if verbose : print('--File already exists')

        relative_path = os.path.relpath(expected_path, file_root_folder)
        relative_path = '//'+relative_path

        lib.filepath = relative_path
    else:
        if verbose : print('Path is already conformed')
    return expected_path,is_new   

def subprocess_safe_copy(src, dst, verbose = verbose):
        ''' Copy a file using a subprocess that open the file and save it in the destination to maintain clean links'''
        function_code = f'''
import bpy
import os
# Save the file to the destination
bpy.ops.wm.save_as_mainfile(filepath=r"{dst}")
#Remap links to local sources folder
sources_folder = os.path.dirname(os.path.dirname(bpy.data.filepath))
for lib in bpy.data.libraries :
    lib_name = os.path.basename(lib.filepath.split('.')[0]) #lib_name
    lib_folder = os.path.join(sources_folder, lib_name) #*absolute*//sources/lib_name (relative)
    new_path = os.path.join(lib_folder, os.path.basename(lib.filepath)) #*absolute*//sources/lib_name/lib_name.blend 
    print("remapping lib path from", lib.filepath, "to", new_path)
    lib.filepath = new_path
#enforce relative links
bpy.ops.file.make_paths_relative()
# Save the file.
bpy.ops.wm.save_as_mainfile(filepath=bpy.data.filepath)
# Quit Blender
bpy.ops.wm.quit_blender()

        '''
                    # Write the function code to a temporary file
        with open("temp_function.py", "w") as file:
            file.write(function_code)
        BLENDER_PATH = bpy.app.binary_path
        # Run Blender with the script

        subprocess.run([BLENDER_PATH, str(src), "-b", "-P", "temp_function.py"])  
        os.remove("temp_function.py")


def guess_library_online_path(lib,check_online=False, verbose = verbose):
    ''' Guess the online path of a library from its name'''
    try :
        blend_name = os.path.basename(lib.filepath)
        filename = blend_name.split('.')[0]
        asset_type = filename.split('_')[2]

        if asset_type in ['CH','CHARACTERS','PR','PROPS','CR','CREATURES']:
            match asset_type :
                case 'CH' : asset_type = 'CHARACTERS'
                case 'PR' : asset_type = 'PROPS'
                case 'CR' : asset_type = 'CREATURES'
            
            ep_name = filename.split('_')[1]
            if ep_name == 'MP' : ep_name = 'MAIN_PACK'  
            ep_folder = os.path.join(online_path, ep_name)
            asset_type_folder = os.path.join(ep_folder, asset_type)

            asset_name = '_'.join(filename.split('_')[3:-1])
            asset_folder = os.path.join(asset_type_folder, asset_name)

            task_name = filename.split('_')[-1]
            match task_name :
                case 'MS' :
                    match asset_type:
                        case 'CHARACTERS' : task_name =  'MODEL_SHEET'
                        case 'CREATURES' : task_name = 'MODEL_SHEET'
                        case 'PROPS' : task_name = 'MODEL_SHEET_PROPS'
            match task_name :
                case 'RIG2D' :
                    match asset_type:
                        case 'CHARACTERS' : task_name =  'RIG_2D_CHARS'
                        case 'CREATURES' : task_name = 'RIG_2D_CHARS'
                        case 'PROPS' : task_name = 'RIG_2D_PROPS'
                case 'GPM' : task_name = 'GPMAT'
            output_folder = os.path.join(asset_folder, task_name, 'OUTPUT')
            online_lib = os.path.join(output_folder, blend_name)
            if verbose : print('GUESS : ', online_lib)
            if check_online:
                if os.path.exists(online_lib):
                    print('File found online')
                else :
                    print('WARNING! Couldnt find online file. Are you connected to the server?')

            return online_lib
        else :
            if verbose : print('Asset type not recognized')
    except Exception as e :
        print('ERROR, IMPOSSIBLE TO GUESS ONLINE PATH, file naming is off:\n', e)

def get_lib_expected_local_paths(lib_path, export_folder):
    sources_folder = os.path.join(export_folder, local_path )

    offline_lib_folder = os.path.join(sources_folder, os.path.basename(lib_path).split('.')[0])

    offline_lib_path = os.path.join(offline_lib_folder, os.path.basename(lib_path))  #absolute offline path

    return offline_lib_path

def write_export_log(log,overwrite = False) :
    if not 'Export Offline Log' in bpy.data.texts.keys() : 
        text_log = bpy.data.texts.new('Export Offline Log')
    else : 
        if not overwrite :
            return text_log
        text_log = bpy.data.texts['Export Offline Log']
        text_log.clear()
    text_log.write('------------------- Export Offline Log : ----------' + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + '\n')
    json_log = json.dumps(log)
    text_log.write(json_log)
    return text_log

def get_server_folder_from_name(name, verbose = verbose):
    project_mark = name.split('_')[0]
    
    path = None
    
    match project_mark :
        case 'EWI':
            if verbose : print('-Ewilan project')
            path = online_path
            name_parts = name.split('_')
            folder_type = name_parts[1]
            match folder_type :
                case 'MP':
                    if verbose : print('--Main Pack folder assets')
                    
                    asset_type = name_parts[2]
                    asset_folder = get_asset_type_folder(asset_type)

                    if asset_folder:
                        task_type = name_parts[-1]
                        task_folder = get_asset_task_folder(task_type, asset_type)

                        if task_folder:
                            if len(name_parts) > 4:  # Ensure there are enough parts to slice
                                object_name = '_'.join(name_parts[3:-1])
                            else:
                                object_name = name_parts[3]  # If there's only one element, use it directly
                            if verbose : print('-----Object name:', object_name)
                            path = os.path.join(path, 'ASSETS', 'MAIN_PACK', asset_folder, object_name, task_folder, 'OUTPUT')
                        else:
                            path=None
                    else:
                        path=None
                case 'LIB':
                    if verbose : print('--Library folder')
                    task_type = name_parts[2]
                    match task_type :
                        case 'PLT-TASKS':
                            if verbose : print('---Palette tasks')
                            task_folder = 'PALETTE_TASKS'
                            subtask_type = name_parts[3]
                            subsubtask_type = None
                            match subtask_type :                                
                                case 'ANIM':
                                    if verbose : print('----ANIM')
                                    subtask_folder = 'ANIM'
                                    pass
                                case 'LAY':
                                    if verbose : print('----LAY_BG')
                                    subtask_folder = 'LAY_BG'
                                    pass
                                case 'MS':
                                    if verbose : print('----MS')
                                    subtask_folder = 'MS'
                                    subsubtask_type = name_parts[4] 
                                    pass
                                case 'PROPS':
                                    if verbose : print('----PROPS_BG')
                                    subtask_folder = 'PROPS_BG'
                                    subsubtask_type = name_parts[-1]
                                    pass
                                case 'RIG':
                                    if verbose : print('----RIG')
                                    subtask_folder = 'RIG'
                                    subsubtask_type = name_parts[-1]
                                    pass
                                case _:
                                    if verbose : print(f'----Subtask type {subtask_type} not recognized')
                                    path = None
                            if subsubtask_type:
                                subsubtask_folder= None
                                match subsubtask_type :
                                    case 'GPM':
                                        if verbose : print('-----GPM')
                                        subsubtask_folder = 'GPMAT'
                                        pass
                                    case 'PLT':
                                        if verbose : print('-----PLT')
                                        subsubtask_folder = 'PALETTE'
                                        pass
                                    case _:
                                        if verbose : print(f'-----Subsubtask type {subsubtask_type} not recognized')
                                        path = None
                                if subsubtask_folder:
                                    path = os.path.join(path,'ASSETS' ,'LIBRARIES', task_folder, subtask_folder, subsubtask_folder, 'OUTPUT')
                            else:
                                if subtask_folder:
                                    path = os.path.join(path,'ASSETS' ,'LIBRARIES', task_folder, subtask_folder, 'OUTPUT')
                        case 'TPL':
                            if verbose : print('---Template tasks')
                            subtask_type = name_parts[3]
                            subtask_folder = None
                            match subtask_type :
                                case 'ANIM':
                                    if verbose : print('----ANIM')
                                    subtask_folder = 'ANIM'
                                    pass
                                case 'BRUSHES':
                                    if verbose : print('----BRUSHES')
                                    subtask_folder = 'BRUSHES'
                                    pass
                                case 'COMPO':
                                    if verbose : print('----COMPO')
                                    subtask_folder = 'COMPO'
                                    pass
                                case 'GPMAT':
                                    if verbose : print('----GPMAT')
                                    subtask_folder = 'GPMAT'
                                    pass
                                case 'LAY':
                                    if verbose : print('----LAY_BG')
                                    subtask_folder = 'LAY_BG'
                                    pass
                                case 'MODE':
                                    if verbose : print('----MODE_3D')
                                    subtask_folder = 'MODE_3D'
                                    pass
                                case 'MS': #issue with MS and MS props
                                    if verbose : print('----MS or MS_PROPS')
                                    if name_parts[3]!='PROPS':
                                        subtask_folder = 'MS'
                                    else:
                                        subsubtask_folder = 'MS_PROPS'
                                    pass
                                case 'PAINT':
                                    if verbose: print('----PAINT_BG')
                                    subtask_folder = 'PAINT_BG'
                                case 'PALETTE':
                                    if verbose: print('----PALETTE')
                                    subtask_folder = 'PALETTE'
                                case 'RIG':
                                    if verbose: print('----RIG_2D or RIG_2D_PROPS')
                                    if name_parts[4]!='PROPS':
                                        subtask_folder = 'RIG_2D'
                                    else:
                                        subsubtask_folder = 'RIG_2D_PROPS'
                                case _:
                                    if verbose : print(f'----Subtask type {subtask_type} not recognized')
                                    path = None
                            if subtask_folder:
                                path = os.path.join(path,'ASSET' ,'LIBRAIRIES','TEMPLATES', subtask_folder,'TEMPLATE', 'OUTPUT')



                        case _:
                            if verbose : print(f'---Task type {task_type} not recognized')
                            path = None

                    pass
                case _ if folder_type.startswith('EP'):
                    if verbose : print('--Episode folder')
                    episode = folder_type
                    sequence = name_parts[2]
                    shot = name_parts[3]
                    if sequence.startswith('SQ') and shot.startswith('SH'):
                        if verbose : print('---Sequence and shot found')
                        task_type = name_parts[-1]
                        task_folder = None
                        match task_type :
                            case 'ANIM':
                                if verbose : print('----Task anim')
                                task_folder = 'ANIMATION'
                                pass
                            case 'BKG':
                                if verbose : print('----Task blocking')
                                task_folder = 'BLOCKING'
                                pass
                            case 'LAYBG':
                                if verbose : print('----Task layout blocking')
                                task_folder = 'LAYOUT_BG'
                                pass
                            case 'LAYPOS':
                                if verbose : print('----Task layout posing')
                                task_folder = 'LAYOUT_POSING'
                                pass
                            case 'BG':
                                if verbose : print('----Task BG paint')
                                task_folder = 'BG_PAINT'
                                pass
                            case _:
                                if verbose : print(f'----Task type {task_type} not recognized')
                                path = None
                        if task_folder:
                            path = os.path.join(path, episode, sequence, shot, task_folder, 'OUTPUT')
                        else:
                            path = None
                    else:
                        if verbose : print('---Sequence or shot not found, its an episiodic asset')
                        asset_type = name_parts[2]
                        asset_folder = get_asset_type_folder(asset_type)

                        if asset_folder:
                            task_type = name_parts[-1]
                            task_folder = get_asset_task_folder(task_type, asset_type)

                            if task_folder:
                                object_name = '_'.join(name_parts[3: -1])
                                if verbose : print('-----Object name:', object_name)
                                path = os.path.join(path, 'ASSETS', episode, asset_folder, object_name, task_folder, 'OUTPUT')
                            else:
                                path=None
                        else:
                            path=None
                case _:
                    if verbose : print(f'--Folder type {folder_type} not recognized')
                    path = None
        case _:
            if verbose : print('Project not recognized')
    if not path:
        if verbose : print('WARNING!!! Couldnt find server path from ', name)
    else :
        if verbose : print('Server path guessed :', path)
    return path

def get_asset_task_folder(task_type, asset_type, verbose=verbose):
    match task_type :
        case 'RIG2D':
            if verbose : print('----TASK 2D Rigging')
            match asset_type:
                case 'CHARACTERS':
                    task_folder = 'RIG_2D_CHARS'
                case 'PROPS':
                    task_folder = 'RIG_2D_PROPS'
                case _: #main pack asset can have CH instead of CHARACTERS
                   task_folder = 'RIG_2D_CHARS' 
            pass
        case 'GPM':
            if verbose : print('----TASK GPM')
            task_folder = 'GPMAT'
            pass
        case 'MS':
            if verbose : print('----TASK model sheet ***')
            print('***', asset_type)
            match asset_type:
                case 'PROPS':
                    task_folder = 'MODEL_SHEET_PROPS'
                case _:
                    task_folder = 'MODEL_SHEET'
            pass
        case 'PL':
            if verbose : print('----TASK palette')
            task_folder = 'PALETTE'
            pass
        case _:
            if verbose : print(f'----Task type {task_type} not recognized')
            task_folder = None
    return task_folder

def get_asset_type_folder(asset_type, verbose=verbose):
    match asset_type :
        case 'CH'|'CHARACTERS':
            if verbose : print('---Characters folder')
            asset_folder = 'CHARACTERS'
            pass
        case 'PR'|'PROPS':
            if verbose : print('---Props folder')
            asset_folder = 'PROPS'
            pass
        case 'CR'|'CREATURES':
            if verbose : print('---Creatures folder')
            asset_folder = 'CREATURES'
            pass
        case 'BG'|'BACKGROUNDS':
            if verbose : print('---Backgrounds folder')
            asset_folder = 'BG'
            pass
        
        case _:
            if verbose : print(f'---Asset type {asset_type} not recognized')
            asset_folder = None
    return asset_folder