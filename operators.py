
import subprocess
import bpy
import os
from bpy.types import Operator
from .export_log_utils import conform_log_library_path, conform_sub_library_path, get_log,local_path, clear_log_libs
from .common.import_utils import ensure_lib
mandatory_libs = [
    "gazu",
    ]
for lib in mandatory_libs :
    has_lib = ensure_lib(lib)
    if not has_lib :
        raise Exception('Error: Could not install "' + lib + '\n Try again and if this error persists install dependencies manually')
import gazu
from .kitsu_connection_checker import check_connection

PROJECT_NAME = 'EWILAN'


class EWILIB_LIB(bpy.types.PropertyGroup):
    name : bpy.props.StringProperty(default = 'name') # display name
    full_name : bpy.props.StringProperty(default = 'lib name')  #Full path name
    path : bpy.props.StringProperty(default='xxxx') #folder path to asset lib

class EWILIB_UL_LIBS(bpy.types.UIList):
    
    def draw_item(self, _context, layout, _data, item, icon, _active_data, _active_propname, _index):     
        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            row = layout.row(align=True)
            row.prop(item, "name", text="", emboss=False, icon_value=icon) 
        elif self.layout_type == 'GRID':
            layout.alignment = 'CENTER'
            layout.prop(item, "name", text="", emboss=False, icon_value=icon) 

class EWILIB_MT_ASSETMENU(bpy.types.Menu):
    bl_label = "Select asset"
    bl_idname = "EWILIB_MT_ASSETMENU"
    
    def draw(self, context):
        addon_prefs = context.preferences.addons[__package__].preferences
        layout = self.layout
        for i in get_ass_names_from_dic(context.scene.ewilib.kitsu_dic, addon_prefs.selected_episode, addon_prefs.selected_asset_type ):
            row = layout.row()
            ops = row.operator('ewilib.add_lib', text = str(i))
            ops.assetname = addon_prefs.selected_episode +'_'+ addon_prefs.selected_asset_type +'_'+ str(i)
        
class EWILIB_MT_ASSETTYPEMENU(bpy.types.Menu):
    bl_label = "Select asset type"
    bl_idname = "EWILIB_MT_ASSETTYPEMENU"
    
    def draw(self, context):
        addon_prefs = bpy.context.preferences.addons[__package__].preferences
        layout = self.layout
        for i in get_ass_type_from_dic(context.scene.ewilib.kitsu_dic, addon_prefs.selected_episode):
            row = layout.row()
            ops = row.operator('ewilib.add_lib_menu', text = i)
            ops.next_menu_bl_idname = 'EWILIB_MT_ASSETMENU'
            ops.prop_name = 'selected_asset_type'
            ops.prop_value = i

class EWILIB_MT_EPISODEMENU(bpy.types.Menu):
    bl_label = "Select episode"
    bl_idname = "EWILIB_MT_EPISODEMENU"
    
    def draw(self, context):
        layout = self.layout
        for ep in get_ep_names_from_dic(context.scene.ewilib.kitsu_dic) : 
            row = layout.row()
            ops = row.operator('ewilib.add_lib_menu', text = ep)
            ops.next_menu_bl_idname = 'EWILIB_MT_ASSETTYPEMENU'
            ops.prop_name = 'selected_episode'
            ops.prop_value = ep

class EWILIB_OT_REMOVE_LIB(Operator):
    bl_idname = "ewilib.remove_lib"
    bl_label = "remove library"
    def execute(self,context):
        preferences = context.preferences
        addon_prefs = preferences.addons[__package__].preferences
        libs = addon_prefs.ewi_libs
        if len(libs) == 0 :
            return{'CANCELLED'}
        i = addon_prefs.ewi_active_lib
        remove_asset_lib(libs[i])
        addon_prefs.ewi_libs.remove(i)
        return{'FINISHED'}

class EWILIB_OT_ADD_LIB(Operator):
    bl_idname = "ewilib.add_lib"
    bl_label = "add library"
    assetname : bpy.props.StringProperty()
    def execute(self,context):
        #create new ewi_lib item
        print(self.assetname)
        addon_prefs = context.preferences.addons[__package__].preferences
        new_lib = addon_prefs.ewi_libs.add()
        new_lib.full_name = self.assetname

        new_lib.name = self.assetname.split('_')[-1:][0]
        #determine folderpath
        set_lib_folder_path(new_lib)
        #load asset library
        import_asset_lib(new_lib)

        return{'FINISHED'}

class EWILIB_OT_ADD_LIB_MENU(Operator):
    '''browse the kitsu/local database to add asset library'''
    bl_idname = "ewilib.add_lib_menu"
    bl_label = "add library by browsing"
    next_menu_bl_idname : bpy.props.StringProperty()
    prop_name : bpy.props.StringProperty()
    prop_value : bpy.props.StringProperty()
    def execute(self,context):
        preferences = context.preferences
        addon_prefs = preferences.addons[__package__].preferences
        #print(addon_prefs, self.prop_name, self.prop_value)
        setattr(addon_prefs, self.prop_name, self.prop_value)
        bpy.ops.wm.call_menu(name=self.next_menu_bl_idname)

        return {'FINISHED'}

class EWILIB_OT_CONF_LIB(Operator):
    '''conform all linked libraries to local path - Ctrl + clic to ignore logged lib path'''
    bl_idname = "ewilib.conf_lib"
    bl_label = "conform libraries"
    clear_log : bpy.props.BoolProperty(default = False)
    def invoke(self, context, event):
        self.clear_log = event.ctrl
        return self.execute(context)
    def execute(self,context):
        preferences = context.preferences
        addon_prefs = preferences.addons[__package__].preferences
        export_log = get_log(force_creation = True)
        if export_log is None :
            self.report({'ERROR'}, message = 'export log not found')
            return {'CANCELLED'}
        if self.clear_log :
            print('######################CLEAR LOG')
            export_log = clear_log_libs(export_log)
        conform_log_library_path(export_log)
        #bpy.ops.ewilib.conf_recurs_lib_path()
        return{'FINISHED'}

class EWILIB_OT_CONF_RECURS_LIB_PATH(Operator):
    '''recursively conform the selected library 
    and sub library path to local path in a subprocess
    assuming they are already existing in source folder'''
    bl_idname = "ewilib.conf_recurs_lib_path"
    bl_label = "conform library path recursively"
    is_sub_lib : bpy.props.BoolProperty(default = False)
    is_recursive : bpy.props.BoolProperty(default = True)

    def execute(self,context):

        #launch a subprocess to conform all sub libraries

        if  self.is_recursive :

            treated= []
            for lib in bpy.data.libraries :
                # Iterate through second-degree libraries
                if lib not in treated :

                    print('conforming', lib.name, 'sub libraries')
                    function_code = '''
import bpy
bpy.ops.ewilib.conf_recurs_lib_path(is_sub_lib = True, is_recursive = False)
#save the file
bpy.ops.wm.save_as_mainfile(filepath=bpy.data.filepath)
#quit blender
bpy.ops.wm.quit_blender()
                    '''
                                # Write the function code to a temporary file
                    with open("temp_function.py", "w") as file:
                        file.write(function_code)
                    BLENDER_PATH = bpy.app.binary_path
                    # Run Blender with the script
                    parent_abs_path = bpy.path.abspath(lib.filepath)
                    subprocess.run([BLENDER_PATH, str(parent_abs_path), "-b", "-P", "temp_function.py"])   
                    treated.append(lib)
                    
        # if self.is_sub_lib :
            #conform sub library path
        conform_sub_library_path(is_sub_lib = self.is_sub_lib)  
        return{'FINISHED'}
    
class EWILIB_PROPS(bpy.types.PropertyGroup):
    kitsu_dic = {}



class EWILIB_OT_REFRESH_KITSU_DIC(Operator):
    '''refresh the kitsu/local database'''
    bl_idname = "ewilib.refresh_dic"
    bl_label = "refresh kitsu assets"
    
    def execute(self,context):
        kitsu_connection = check_connection()
        if kitsu_connection != True :
            self.report({'INFO'}, message = kitsu_connection)
            return {'CANCELLED'}
         
        dic = get_kitsu_asset_dic(PROJECT_NAME)
        for k in dic.keys() :
            context.scene.ewilib.kitsu_dic[k] = dic[k]
        return {'FINISHED'}
    
class EWILIB_OT_ADD_LIB_BUTTON(Operator):
    '''browse the kitsu/local database to add asset library'''
    bl_idname = "ewilib.add_lib_button"
    bl_label = "add library by browsing"
    def invoke(self, context, event):
        dic = get_kitsu_asset_dic(PROJECT_NAME)
        for k in dic.keys() :
            context.scene.ewilib.kitsu_dic[k] = dic[k]
        return self.execute(context)
    def execute(self,context):
        bpy.ops.ewilib.add_lib_menu(next_menu_bl_idname = 'EWILIB_MT_EPISODEMENU')

        return {'FINISHED'}
    
class EWILIB_OT_ADD_LIB_FOR_SHOT(Operator):
    '''determine shot from filename, ask casting to kitsu, and add asset libraries'''
    bl_idname = "ewilib.add_lib_for_shot"
    bl_label = "add libraries from shot casting"
    
    def execute(self,context):
        kitsu_connection = check_connection()
        if kitsu_connection != True :
            self.report({'INFO'}, message = kitsu_connection)
            return {'CANCELLED'}
        
        # Get the current blend file name
        current_file = bpy.data.filepath

        # Extract the file name from the full path
        file_name = os.path.basename(current_file)

        # Split the file name using "_"
        try :
            proj_code, ep, sq, sh, task, worker = file_name.split("_")
        except :
            self.report({'ERROR'}, message = 'The file name does not allow to guess the current shot - aborted')
            return {'CANCELLED'}
        #print(proj_code)
        #print(ep)
        #print(sq)
        #print(sh)
        #print(task)
        #print(worker.split(".")[0])

        try :
            project = gazu.project.get_project_by_name("EWILAN")
            episode = gazu.shot.get_episode_by_name(project, ep)
            sequence = gazu.shot.get_sequence_by_name(project,sq,episode)
            shot = gazu.shot.get_shot_by_name(sequence, sh)
            casting = gazu.casting.get_shot_casting(shot)
        except :
            self.report({'ERROR'}, message = 'Unable to get current shot from filename')
            return {'CANCELLED'}
        
        asset_list = [] #EP_ASSTYPE_ASSET

        # Iterate through the casting list
        for entry in casting:
            #print(entry)
            if entry["episode_id"] == None :
                asset_ep = 'MAIN-PACK'
            else :
                asset_ep = gazu.shot.get_episode(entry["episode_id"])['name']
            asset_name = entry["asset_name"].replace('_','-')
            asset_type_name = entry.get('asset_type_name', '')
            asset_list.append(asset_ep + '_' + asset_type_name + '_' + asset_name)
        
        
        
        # Deletes the existing libraries TODO : ignore BANK ANIM
        addon_prefs = context.preferences.addons[__package__].preferences
        reversed_libs = []
        for i, lib in enumerate(addon_prefs.ewi_libs):
            reversed_libs.append((i,lib))
        for i, lib in reversed(reversed_libs) :
            remove_asset_lib(lib)
            addon_prefs.ewi_libs.remove(i) ###nedd index

        #add libs
        for asset in asset_list :
            bpy.ops.ewilib.add_lib(assetname = asset)  
        if addon_prefs.load_bank :
            from .handlers import load_bank
            load_bank()

        return {'FINISHED'}

class EWILIBS_PT_PANEL(bpy.types.Panel): 
    bl_label = "Library selector"
    bl_idname = "EWILIBS_PT_PANEL"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Lib Selector"
    
    def draw(self, context):
        draw_lib_selector(self, context, self.layout)
 
def draw_lib_selector(self, context, layout):
    addon_prefs = context.preferences.addons[__package__].preferences
    row = layout.row()
    if not addon_prefs.use_client_tool:
        if not os.path.exists(addon_prefs.base_path):
            row.label(text= 'WARNING, your base path is incorrect', icon = 'ERROR')
            row = layout.row()
            row.prop(addon_prefs, "base_path", text = '')
            
            row.prop(addon_prefs, 'use_client_tool', text = 'I work with client tool')
            row = layout.row()
    row.label(text='libraries selector')
    row = layout.row()
    col = row.column()
    col.template_list("EWILIB_UL_LIBS", "", addon_prefs, "ewi_libs", addon_prefs, "ewi_active_lib", rows=4, sort_lock=True)
    col2 = row.column(align=True) 
    col2.operator('ewilib.add_lib_for_shot', text = '', icon = 'PLUS')
    #auto select from ui
    if addon_prefs.ewi_last_active_lib != addon_prefs.ewi_active_lib :
        select_lib(addon_prefs.ewi_libs[addon_prefs.ewi_active_lib])
        addon_prefs.ewi_last_active_lib = addon_prefs.ewi_active_lib

    col2.operator('ewilib.add_lib_button', text = '', icon = 'ADD')
    col2.operator('ewilib.remove_lib', text = '', icon = 'REMOVE')
    #col2.operator('ewilib.refresh_dic', text = '', icon = 'FILE_REFRESH')

    row = layout.row()
    row.prop(addon_prefs, "auto_switch_lib", text = 'Auto switch libraries')
    #row = layout.row()
    row.prop(addon_prefs, "auto_switch_pal", text = 'Auto switch palettes')
    row = layout.row()
    row.prop(addon_prefs, "auto_load_kitsu_assets", text = 'Auto load kitsu assets')
    #row = layout.row()
    row.prop(addon_prefs, "load_bank", text = 'Load anim bank')

    if addon_prefs.use_client_tool:
        row = layout.row()
        row.operator('ewilib.conf_lib', text = 'Conform linked libraries')


def import_asset_lib(lib):
    i = len(bpy.context.preferences.filepaths.asset_libraries)
    if lib.name not in [l.name for l in bpy.context.preferences.filepaths.asset_libraries]:
        bpy.ops.preferences.asset_library_add(directory =lib.path, display_type ='THUMBNAIL')
        new_lib = bpy.context.preferences.filepaths.asset_libraries[i]
        new_lib.name = lib.name

def remove_asset_lib(lib):
    for i , l in enumerate(bpy.context.preferences.filepaths.asset_libraries):
        if lib.name == l.name :
            bpy.ops.preferences.asset_library_remove(index=i)

def clear_libs():
    for i, lib in reversed(enumerate(bpy.context.preferences.addons[__package__].preferences.ewi_libs)) :
        remove_asset_lib(lib)
        bpy.context.preferences.addons[__package__].preferences.ewi_libs.remove(i)

def set_lib_folder_path(lib): #TODO Offline mode
    addon_prefs = bpy.context.preferences.addons[__package__].preferences
    episode, asset_type, asset_name = lib.full_name.split('_')
    if not addon_prefs.use_client_tool :
        match asset_type :
            case 'CHARACTERS' :
                final_task = 'RIG_2D_CHARS' #TODO get the real final task (props rig / creature 2D or 3D / character 2D rig)
            case 'PROPS' :
                final_task = 'RIG_2D_PROPS'
            case 'CREATURES' :
                final_task = 'RIG_2D_CHARS'
            case _ :
                final_task = None

        #checker l'existence des dossiers output dans rig2D et 3D, possiblement les 2 !
        asset_folderpath = addon_prefs.base_path + '\\ASSETS\\' + episode.replace('-','_') + "\\" + asset_type + "\\" + asset_name.replace('-','_') + "\\" + final_task
        if addon_prefs.get_last_revision :
            revisions = [folder for folder in os.listdir(asset_folderpath) if folder.startswith('v')]
            revisions.sort(reverse=True) 
            lib.path = asset_folderpath + "\\" + revisions[0]
            
        else :
            lib.path = asset_folderpath + "\\OUTPUT"
        return lib.path
    else :
        final_asset_type = asset_type
        if episode.replace('-','_') == 'MAIN_PACK':
            episode = 'MP'
            match asset_type :
                case 'CHARACTERS' :
                    final_asset_type = 'CH'
                case 'PROPS' :
                    final_asset_type = 'PR'
                case 'CREATURES' :
                    final_asset_type = 'CR'
        sources_folder = os.path.join(os.path.dirname(bpy.data.filepath), 'sources')
        asset_folder =  'EWI_' + episode.replace('-','_') + "_" + final_asset_type + "_" + asset_name.replace('-','_')  + '_RIG2D'
        lib.path = os.path.join(sources_folder, asset_folder)
        return lib.path
        

def merge_dicts(d1, d2):
    for key in d2:
        if key in d1 and isinstance(d1[key], dict) and isinstance(d2[key], dict):
            merge_dicts(d1[key], d2[key])
        else:
            d1[key] = d2[key]
    return d1

def sort_dict(d):
    sorted_dict = {}
    for key in sorted(d.keys()):
        if isinstance(d[key], dict):
            sorted_dict[key] = sort_dict(d[key])
        else:
            sorted_dict[key] = d[key]
    return sorted_dict 

def get_kitsu_asset_dic(project_name):
    project = gazu.project.get_project_by_name(project_name)
    result = {}
    asset_types = {}
    all_assets = gazu.client.fetch_all(f'assets?project_id={project["id"]}')
    for asset in all_assets:
        if asset['entity_type_id'] not in asset_types.keys():
            asset_types[asset['entity_type_id']] = gazu.entity.get_entity_type(asset['entity_type_id'])['name']
        asset_type = asset_types[asset['entity_type_id']]
        if asset_type in ['CHARACTERS', 'PROPS', 'CREATURES']:
            
            asset_episode = asset.get('data').get('origin')
            
            
            #if asset["episode_id"] == None :
            #    asset_episode = asset.get('data').get('origin')
            
           
           
            if asset_episode and (asset_episode.startswith('EP') or asset_episode == 'MAIN_PACK') :
                asset_name = asset['name']
                result = merge_dicts(result.copy(), {asset_episode.replace('_', '-') : {asset_type.replace('_', '-') : {asset_name.replace('_', '-') : ''}}})

    for ep in result.keys():
        for a_type in result[ep].keys():
            list_assets = list(result[ep][a_type].keys())
            result[ep][a_type] = list_assets
    
    return sort_dict(result)

def get_ep_names_from_dic(dic):
    return [k for k in dic.keys()]
def get_ass_type_from_dic(dic,ep):
    return [k for k in dic[ep].keys()]
def get_ass_names_from_dic(dic,ep,ass_type):
    return [i for i in dic[ep][ass_type]]


def old_get_kitsu_asset_dic(project_name):
    result = {}
    eps = get_episodes_names(project_name)
    for episode_name in eps:
        print(episode_name)
        result_ep = {}
        ass_typ = get_asset_types(project_name)
        for asset_type in [a for a in ass_typ if a in ['CHARACTERS', 'PROPS', 'CREATURES']] :
            print('---',asset_type)
            result_ep[asset_type] = get_asset_names(project_name, episode_name, asset_type)
        result[episode_name] = result_ep
    return result





def get_episodes_names(project_name):
    project = gazu.project.get_project_by_name(project_name)
    result = ['MAIN-PACK']
    for item in gazu.shot.all_episodes_for_project(project):
        result.append(item['name'])
    return result

def get_asset_types(project_name):
    project = gazu.project.get_project_by_name(project_name)
    return [at['name'] for at in gazu.asset.all_asset_types_for_project(project)]

def get_asset_names(project_name, episode_name, asset_type):
    project = gazu.project.get_project_by_name(project_name)
    assets_names = []
    if episode_name == 'MAIN-PACK':
        for asset in get_main_pack_assets(project):
            assets_names.append(asset['name'].replace('_','-'))
    else :
        episode = gazu.shot.get_episode_by_name(project, episode_name)
        for asset in gazu.asset.all_assets_for_episode(episode) :
            if gazu.asset.get_asset_type_from_asset(asset)['name'] == asset_type :
                assets_names.append(asset['name'].replace('_','-'))
    return assets_names

def get_main_pack_assets(pr):
    mp_assets = []
    _all = gazu.asset.all_assets_for_project(pr)
    for asset in _all:
        try :
            if asset["data"]["origin"] == "MAIN_PACK":
                mp_assets.append(asset)
        except:
            #print(asset["name"], " have no origin") 
            pass
    return mp_assets

def select_lib(lib):
    asset_name = lib.name
    if asset_name in [lib.name for lib in bpy.context.preferences.filepaths.asset_libraries] :
        for area in bpy.context.screen.areas :
            if area.type == 'FILE_BROWSER' and area.ui_type == 'ASSETS': 
                area.spaces[0].params.asset_library_ref = asset_name
                break
