# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "Ewi_Libraries",
    "author" : "Tom VIGUIER",
    "description" : "",
    "blender" : (3, 6, 0),
    "version" : (0, 0, 7),
    "location" : "",
    "warning" : "",
    "category" : "Andarta"
}
import bpy
from .operators import *
from .handlers import *
import os




class EWI_LIB_AddonPref(bpy.types.AddonPreferences):
    bl_idname = __name__

    auto_load_kitsu_assets : bpy.props.BoolProperty(default = False)
    auto_switch_lib : bpy.props.BoolProperty(default = True)
    auto_switch_pal : bpy.props.BoolProperty(default = False)
    get_last_revision : bpy.props.BoolProperty(default = False)
    load_bank :  bpy.props.BoolProperty(default = True, update = load_bank_update)

    ewi_libs : bpy.props.CollectionProperty(type = EWILIB_LIB)
    ewi_active_lib : bpy.props.IntProperty(default = 0)
    ewi_last_active_lib : bpy.props.IntProperty(default = 0)
    selected_episode : bpy.props.StringProperty()
    selected_asset_type : bpy.props.StringProperty()
    selected_asset : bpy.props.StringProperty()
    list = []
    use_client_tool : bpy.props.BoolProperty(default = False)
    base_path : bpy.props.StringProperty(default = "\\\\srvfiles\\projects\\ewilan\\FAB\\", subtype = 'DIR_PATH')

    def draw(self, context):
        layout = self.layout
        row = layout.row() 
        row.prop(self, 'use_client_tool', text = 'I work with client tool')
        if not self.use_client_tool :
            row = layout.row() 
            icon = 'NONE'
            if not os.path.exists(self.base_path):
                icon = 'ERROR'
            row.prop(self, 'base_path', text = 'base path ', emboss = True, icon = icon)
            row = layout.row()
        
        draw_lib_selector(self,context,layout)
        

cls = [
       EWILIB_UL_LIBS,
       EWILIB_LIB,
       EWILIB_MT_EPISODEMENU,
       EWILIB_MT_ASSETTYPEMENU,
       EWILIB_MT_ASSETMENU,
       EWILIB_OT_ADD_LIB,
       EWILIB_OT_REMOVE_LIB,
       EWILIB_OT_ADD_LIB_MENU,
       EWILIB_OT_ADD_LIB_FOR_SHOT,
       EWILIBS_PT_PANEL,
       EWILIB_OT_ADD_LIB_BUTTON,
       EWILIB_PROPS,
       EWILIB_OT_CONF_LIB,
       #EWILIB_OT_REFRESH_KITSU_DIC,
       EWI_LIB_AddonPref,
       EWILIB_OT_CONF_RECURS_LIB_PATH,
       ]
def register():
    for cl in cls :
        bpy.utils.register_class(cl)
    bpy.types.Scene.ewilib = bpy.props.PointerProperty(type = EWILIB_PROPS)

    bpy.app.handlers.depsgraph_update_post.append(on_object_change)
    bpy.app.handlers.load_pre.append(on_load_handler)
    on_load_handler(None,None)

def unregister():
    for cl in cls :
        bpy.utils.unregister_class(cl)
    del bpy.types.Scene.ewilib
    if on_object_change in bpy.app.handlers.depsgraph_update_post :
        bpy.app.handlers.depsgraph_update_post.remove(on_object_change)
    if on_load_handler in bpy.app.handlers.load_post:
        bpy.app.handlers.load_pre.remove(on_load_handler)